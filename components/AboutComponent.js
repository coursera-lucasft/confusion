import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView } from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import { LEADERS } from '../shared/leaders';
import { HISTORY } from '../shared/assignment.js'
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';


const mapStateToProps = state => {
  return {
    leaders: state.leaders
  }
}

function History(props) {
  const { history } = props
  const { text } = history
  if (history !== null) {
    return (
      <Text>
        {text}
      </Text>
    )
  }
  else {
    return (<View></View>);
  }

}

class About extends Component {
  constructor(props) {
    super(props)
    this.state = {
      leaders: LEADERS,
      history: HISTORY
    }
  }

  static navigationOptions = {
    title: 'About Us'
  }

  render() {
    const renderLeader = ({ item, index }) => {
      return (
        <ListItem
          key={index}
          title={item.name}
          subtitle={item.description}
          hideChevron={true}
          leftAvatar={{ source: { uri: baseUrl + item.image } }}
        />
      );
    };

    if (this.props.leaders.isLoading) {
      return (
        <ScrollView>
          <History history={this.state.history} />
          <Card
            title='Corporate Leadership'>
            <Loading />
          </Card>
        </ScrollView>
      );
    }
    else if (this.props.leaders.errMess) {
      return (
        <ScrollView>
          <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
            <History history={this.state.history} />
            <Card
              title='Corporate Leadership'>
              <Text>{this.props.leaders.errMess}</Text>
            </Card>
          </Animatable.View>
        </ScrollView>
      );
    }
    else {
      return (
        <ScrollView>
          <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
            <History history={this.state.history} />
            <Card
              title='Corporate Leadership'>
              <FlatList
                data={this.props.leaders.leaders}
                renderItem={renderLeader}
                keyExtractor={item => item.id.toString()}
              />
            </Card>
          </Animatable.View>
        </ScrollView>
      );
    }
  }
}

export default connect(mapStateToProps)(About);