import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList, Modal, StyleSheet, Button, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, Rating, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    favorites: state.favorites
  }
}

const mapDispatchToProps = dispatch => ({
  postFavorite: (dishId) => dispatch(postFavorite(dishId)),
  postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
})

function RenderComments(props) {

  const comments = props.comments;

  const renderCommentItem = ({ item, index }) => {

    return (
      <View key={index} style={{ margin: 10 }}>
        <Text style={{ fontSize: 14 }}>{item.comment}</Text>
        <Rating
          imageSize={12}
          ratingCount={5}
          startingValue={item.rating}
          style={{ paddingVertical: 10 }}
        />
        <Text style={{ fontSize: 12 }}>{'-- ' + item.author + ', ' + item.date} </Text>
      </View>
    );
  };

  return (
    <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
      <Card title='Comments' >
        <FlatList
          data={comments}
          renderItem={renderCommentItem}
          keyExtractor={item => item.id.toString()}
        />
      </Card>
    </Animatable.View>

  );
}

function RenderDish(props) {

  const dish = props.dish;

  handleViewRef = ref => this.view = ref;


  const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
    if (dx > 280)
      return true;
    else
      return false;
  }

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (e, gestureState) => {
      return true;
    },
    onPanResponderGrant: () => { this.view.rubberBand(1000).then(endState => console.log(endState.finished ? 'finished' : 'cancelled')); },
    onPanResponderEnd: (e, gestureState) => {
      console.log("pan responder end", gestureState);
      if (recognizeDrag(gestureState))
        props.handleRating()

      return true;
    }
  })


  const shareDish = (title, message, url) => {
    Share.share({
      title: title,
      message: title + ': ' + message + ' ' + url,
      url: url
    }, {
      dialogTitle: 'Share ' + title
    })
  }

  if (dish != null) {
    return (
      <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
        ref={this.handleViewRef}
        {...panResponder.panHandlers}>
        <Card
          featuredTitle={dish.name}
          image={{ uri: baseUrl + dish.image }}>
          <Text style={{ margin: 10 }}>
            {dish.description}
          </Text>
          <View style={styles.iconsRow}>
            <Icon
              raised
              reverse
              name={props.favorite ? 'heart' : 'heart-o'}
              type='font-awesome'
              color='#f50'
              onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
            />
            <Icon
              raised
              reverse
              name='pencil'
              type='font-awesome'
              color='#512DA8'
              onPress={() => props.handleRating()}
            />
            <Icon
              raised
              reverse
              name='share'
              type='font-awesome'
              color='#51D2A8'
              style={styles.cardItem}
              onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)} />
          </View>
        </Card>
      </Animatable.View>
    );
  }
  else {
    return (<View></View>);
  }
}

class DishDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      favorites: [],
      rating: 5,
      author: '',
      comment: '',
      showModal: false
    }
  }

  static navigationOptions = {
    title: 'Dish Details'
  }

  markFavorite(dishId) {
    this.props.postFavorite(dishId);
  }


  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  handleRating() {
    this.toggleModal();
  }

  handleChange(event, type) {
    this.setState({
      [type]: event.nativeEvent.text
    })
  }

  handleChangeRating(rating) {
    this.setState({
      rating: rating
    })
  }

  postComment(dishId) {
    const { author, comment, rating } = this.state
    this.props.postComment(dishId, rating, author, comment)
    this.toggleModal();
    this.resetForm();
  }

  resetForm() {
    this.setState({
      rating: 5,
      author: '',
      comment: '',
      showModal: false
    });
  }

  render() {
    const dishId = this.props.navigation.getParam('dishId', '')
    return (
      <ScrollView>
        <Modal animationType={"slide"} transparent={false}
          visible={this.state.showModal}
          onDismiss={() => this.toggleModal()}
          onRequestClose={() => this.toggleModal()}>
          <View style={styles.modal}>
            <Rating
              showRating
              onFinishRating={(rating) => this.handleChangeRating(rating)}
              imageSize={50}
              startingValue={5}
              ratingCount={5}
              style={{ paddingVertical: 10 }}
            />
            <Input
              onChange={(event) => this.handleChange(event, type = 'author')}
              placeholder='Author'
              leftIcon={
                {
                  type: 'font-awesome',
                  name: 'user',
                  iconStyle: { marginRight: 10 }
                }
              }
            />
            <Input
              onChange={(event) => this.handleChange(event, type = 'comment')}
              placeholder='Comment'
              leftIcon={
                {
                  type: 'font-awesome',
                  name: 'comment',
                  iconStyle: { marginRight: 10 }
                }
              }
            />
            <View style={styles.button}>
              <Button
                onPress={() => this.postComment(dishId)}
                color="#512DA8"
                title="Submit"
              />
            </View>
            <View style={styles.button}>
              <Button
                onPress={() => { this.toggleModal(); this.resetForm(); }}
                color="gray"
                title="Cancel"
              />
            </View>
          </View>
        </Modal>
        <RenderDish dish={this.props.dishes.dishes[+dishId]}
          favorite={this.props.favorites.some(el => el === dishId)}
          handleRating={() => this.handleRating()}
          onPress={() => this.markFavorite(dishId)}
        />
        <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />
      </ScrollView>
    );
  }

}

const styles = StyleSheet.create({
  iconsRow: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    margin: 20
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    marginTop: 50
  },
  modal: {
    justifyContent: 'center',
    margin: 20
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DishDetail);