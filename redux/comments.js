import * as ActionTypes from './ActionTypes';

export const comments = (state = { errMess: null, comments: [] }, action) => {
  switch (action.type) {
    case ActionTypes.ADD_COMMENTS:
      return { ...state, errMess: null, comments: action.payload };

    case ActionTypes.ADD_COMMENT:
      const comments = state.comments
      const qntdComments = comments.length
      const payloadComment = { ...action.payload, id: qntdComments + 1 }
      comments.push({ ...payloadComment })
      return {
        ...state, comments
      };

    case ActionTypes.COMMENTS_FAILED:
      return { ...state, errMess: action.payload };

    default:
      return state;
  }
};